const express = require('express');
const router = express.Router();
const { createTaskList, getTaskLists, deleteTaskList } = require('../controllers/taskListController');
const auth = require('../middleware/auth');

router.post('/', auth, createTaskList);
router.get('/', auth, getTaskLists);
router.delete('/:id', auth, deleteTaskList);

module.exports = router;
