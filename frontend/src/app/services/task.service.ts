import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Task } from '../models/task.model';
import { TaskList } from '../models/task-list.model';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  private baseUrl = 'http://localhost:3000/api';

  constructor(private http: HttpClient) {}

  createTaskList(taskList: TaskList): Observable<TaskList> {
    return this.http.post<TaskList>(`${this.baseUrl}/taskLists`, taskList);
  }

  getTaskLists(): Observable<TaskList[]> {
    return this.http.get<TaskList[]>(`${this.baseUrl}/taskLists`);
  }

  deleteTaskList(id: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/taskLists/${id}`);
  }

  createTask(task: Task): Observable<Task> {
    return this.http.post<Task>(`${this.baseUrl}/tasks/${task.taskListId}`, task);
  }

  getTasks(taskListId: string): Observable<Task[]> {
    return this.http.get<Task[]>(`${this.baseUrl}/tasks/${taskListId}`);
  }

  updateTask(task: Task): Observable<Task> {
    return this.http.patch<Task>(`${this.baseUrl}/tasks/${task.taskListId}/${task._id}`, task);
  }

  deleteTask(taskListId: string, taskId: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/tasks/${taskListId}/${taskId}`);
  }
}