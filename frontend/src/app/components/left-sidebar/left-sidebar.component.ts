import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TaskList } from '../../models/task-list.model';
import { TaskService } from '../../services/task.service';


@Component({
  selector: 'app-left-sidebar',
  templateUrl: './left-sidebar.component.html',
  styleUrls: ['./left-sidebar.component.scss']
})
export class LeftSidebarComponent implements OnInit {
  taskLists: TaskList[] = [];
  newTaskListName: string = '';

  @Output() selectedTaskList = new EventEmitter<TaskList>();

  constructor(private taskService: TaskService) {}

  ngOnInit(): void {
    this.loadTaskLists();
  }

  loadTaskLists(): void {
    this.taskService.getTaskLists().subscribe(taskLists => {
      this.taskLists = taskLists;
    }, error => console.error('Error fetching task lists:', error));
  }

  createTaskList(): void {
    if (!this.newTaskListName) {
      alert('Please enter a name for the task list.');
      return;
    }
    let newTaskList = new TaskList(this.newTaskListName, 'userId');  // Adjust according to actual user ID handling
    this.taskService.createTaskList(newTaskList).subscribe(taskList => {
      this.taskLists.push(taskList);
      this.newTaskListName = '';  // Reset the input field
    }, error => console.error('Error creating task list:', error));
  }

  deleteTaskList(taskList: TaskList): void {
    if (taskList._id !== undefined) {  // Explicit check for undefined
      this.taskService.deleteTaskList(taskList._id).subscribe(() => {
        this.taskLists = this.taskLists.filter(tl => tl._id !== taskList._id);
      }, error => console.error('Error deleting task list:', error));
    } else {
      console.error('Attempted to delete a task list without an ID');
    }
  }
  

  selectTaskList(taskList: TaskList): void {
    this.selectedTaskList.emit(taskList);
  }
}
