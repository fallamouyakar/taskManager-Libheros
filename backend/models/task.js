const mongoose = require('mongoose');

const taskSchema = new mongoose.Schema({
  taskListId: { type: mongoose.Schema.Types.ObjectId, ref: 'TaskList', required: true },
  description: { type: String, required: true },
  details: String,
  dueDate: Date,
  completed: { type: Boolean, default: false }
});

module.exports = mongoose.model('Task', taskSchema);
