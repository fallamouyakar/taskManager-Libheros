import { Component, OnInit, Input } from '@angular/core';
import { Task } from '../../models/task.model';
import { TaskService } from '../../services/task.service';


@Component({
  selector: 'app-main-content',
  templateUrl: './main-content.component.html',
  styleUrls: ['./main-content.component.scss']
})
export class MainContentComponent implements OnInit {
  @Input()
  taskListId!: string;
  tasks: Task[] = [];
  newTaskDescription: string = '';

  constructor(private taskService: TaskService) {}

  ngOnInit(): void {
    this.loadTasks();
  }

  loadTasks(): void {
    if (this.taskListId) {
      this.taskService.getTasks(this.taskListId).subscribe(tasks => {
        this.tasks = tasks;
      }, error => {
        console.error('Failed to load tasks:', error);
      });
    }
  }

  addTask(): void {
    const newTask = new Task(this.taskListId, this.newTaskDescription);
    this.taskService.createTask(newTask).subscribe(task => {
      this.tasks.push(task);
      this.newTaskDescription = '';  // Clear input field after adding
    }, error => {
      console.error('Failed to add task:', error);
    });
  }

  toggleTaskCompletion(task: Task): void {
    task.completed = !task.completed;
    this.taskService.updateTask(task).subscribe(() => {
      // Update UI or give user feedback
    }, error => {
      console.error('Failed to update task:', error);
    });
  }
}
