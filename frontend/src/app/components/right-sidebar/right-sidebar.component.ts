import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { TaskService } from '../../services/task.service';
import { Task } from '../../models/task.model';


@Component({
  selector: 'app-right-sidebar',
  templateUrl: './right-sidebar.component.html',
  styleUrls: ['./right-sidebar.component.scss']
})
export class RightSidebarComponent implements OnInit {
  @Input() task!: Task;  // Receives the selected task
  @Output() updateTaskList = new EventEmitter();  // Notify parent to refresh the list

  constructor(private taskService: TaskService) {}

  ngOnInit(): void {}

  updateTask(): void {
    this.taskService.updateTask(this.task).subscribe(
      updatedTask => {
        console.log('Task updated:', updatedTask);
        this.updateTaskList.emit();
      },
      error => console.error('Failed to update task:', error)
    );
  }

  deleteTask(): void {
    if (this.task && this.task._id) {
      this.taskService.deleteTask(this.task.taskListId, this.task._id).subscribe(
        () => {
          console.log('Task deleted');
          this.updateTaskList.emit();
        },
        error => console.error('Failed to delete task:', error)
      );
    }
  }
}
