const express = require('express');
const router = express.Router();
const { createTask, getTasks, updateTask, deleteTask } = require('../controllers/taskController');
const auth = require('../middleware/auth');

// Routes under /api/tasks/:taskListId
router.post('/:taskListId', auth, createTask);
router.get('/:taskListId', auth, getTasks);
router.patch('/:taskListId/:id', auth, updateTask);
router.delete('/:taskListId/:id', auth, deleteTask);

module.exports = router;
