const TaskList = require('../models/taskList');
const Task = require('../models/task');

exports.createTaskList = async (req, res) => {
  try {
    const taskList = new TaskList({ ...req.body, userId: req.userId });
    await taskList.save();
    res.status(201).send(taskList);
  } catch (error) {
    res.status(400).send(error);
  }
};

exports.getTaskLists = async (req, res) => {
  try {
    const taskLists = await TaskList.find({ userId: req.userId });
    res.send(taskLists);
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.deleteTaskList = async (req, res) => {
  try {
    const taskList = await TaskList.findOneAndDelete({ _id: req.params.id, userId: req.userId });
    if (!taskList) return res.status(404).send({ error: 'Task list not found' });

    // Delete all tasks associated with this task list
    await Task.deleteMany({ taskListId: req.params.id });

    res.send(taskList);
  } catch (error) {
    res.status(500).send(error);
  }
};
