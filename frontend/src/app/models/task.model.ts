export class Task {
    _id?: string;
    
    constructor(
      public taskListId: string,
      public description: string,
      public details?: string,
      public dueDate?: Date,
      public completed: boolean = false
    ) {}
  }
  