import { Task } from './task.model';

export class TaskList {
    _id?: string;
    
  constructor(
    public name: string,
    public userId: string,
    public tasks: Task[] = []
  ) {}
}
